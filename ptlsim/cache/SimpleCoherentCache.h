    /*
    * This file is the test class for a simple coherence protocol without any coherencelogic
    * Author: Martin Messer 2015
    */

#ifndef SIMPLE_COHERENCE_LOGIC_H
#define SIMPLE_COHERENCE_LOGIC_H

#include <coherenceLogic.h>

#define UPDATE_SIMPLE_TRANS_STATS(old_state, new_state, mode) \
    if(mode) { /* kernel mode */ \
        state_transition(kernel_stats)[(old_state << 2) | new_state]++; \
    } else { \
        state_transition(user_stats)[(old_state << 2) | new_state]++; \
    }

namespace Memory{

    /* cache states */
    namespace CoherentCache{
        enum SIMPLECacheLineState{
            SIMPLE_INVALID=0,
            SIMPLE_MODIFIED,
            SIMPLE_EXCLUSIVE,
            NUM_SIMPLE_STATES
        };

        /* cache state names (for statistics) */
        static const char* SIMPLEStateNames[NUM_SIMPLE_STATES]={
            "Invalid",
            "Modified",
            "Exclusive"
        };

        class SimpleCoherentCache: public CoherenceLogic
        {
            public:
            SimpleCoherentCache(CacheController * cont, Statable *parent,
            MemoryHierarchy *mem_hierarchy)
            : CoherenceLogic("simple", cont,parent, mem_hierarchy)
            , state_transition("state_trans",this)
            , miss_state("miss_state",this)
            , hit_state("hit_state",this)
            {}

            /*
            * methods we must implement
            */
            void handle_local_hit(CacheQueueEntry *queueEntry);

            void handle_local_miss(CacheQueueEntry *queueEntry);

            void handle_interconn_hit(CacheQueueEntry *queueEntry);

            void handle_interconn_miss(CacheQueueEntry *queueEntry);

            void handle_cache_insert(CacheQueueEntry *queueEntry, W64 oldTag);

            void handle_cache_evict(CacheQueueEntry *queueEntry);

            void complete_request(CacheQueueEntry *queueEntry,
                    Message &message);

            void handle_response(CacheQueueEntry *queueEntry,
                    Message &message);

            bool is_line_valid(CacheLine *line);

            void invalidate_line(CacheLine *line);

			void dump_configuration(YAML::Emitter &out) const;

			/* Statistics */

            StatArray<W64, NUM_SIMPLE_STATES> miss_state;
            StatArray<W64, NUM_SIMPLE_STATES> hit_state;
            StatArray<W64,16> state_transition;
        };
    };
};
#endif // SIMPLE_COHERENCE_LOGIC_H
