    /*
    * This file is the test class for a simple coherence protocol without any coherencelogic
    * Author: Martin Messer 2015
    */

    #include <SimpleCoherentCache.h>

    #include <memoryRequest.h>
    #include <coherentCache.h>

    #include <machine.h>

    using namespace Memory;
    using namespace Memory::CoherentCache;

    void SimpleCoherentCache::handle_local_hit(CacheQueueEntry *queueEntry){

        memdebug("handle local hit\n");

        SIMPLECacheLineState *state=(SIMPLECacheLineState*) (&queueEntry->line->state);
        SIMPLECacheLineState oldstate= *state;
        OP_TYPE type= queueEntry->request->get_type();
        bool kernel=queueEntry->request->is_kernel();

        N_STAT_UPDATE(hit_state,[oldstate]++,kernel);

        if(type==MEMORY_OP_EVICT){
            *state=SIMPLE_INVALID;
	     controller->clear_entry_cb(queueEntry);
	     return;
        }
        if(type==MEMORY_OP_UPDATE && oldstate != SIMPLE_MODIFIED){
            /*
            *  if update from upper cache is received, we must
            *  update all lower caches,when current state is not modified
            */
            queueEntry->dest=controller->get_lower_cont();
            queueEntry->sendTo=controller->get_lower_intrconn();
            queueEntry->eventFlags[CACHE_WAIT_INTERCONNECT_EVENT]++;
            controller->wait_interconnect_cb(queueEntry);
	    return;
        }
		switch(oldstate){
		case SIMPLE_INVALID:
                /*
                * we have to send a cache miss
                */
                N_STAT_UPDATE(miss_state,[oldstate]++,kernel);
                controller->cache_miss_cb(queueEntry);
                break;
        case SIMPLE_MODIFIED:
                /*
                *nothing to do , send response
                */
                queueEntry->sendTo=queueEntry->sender;
                controller->wait_interconnect_cb(queueEntry);
        break;
        case SIMPLE_EXCLUSIVE:
                /*
                * we have to change line state to modified
                */
                if(type==MEMORY_OP_READ){
                queueEntry->sendTo=queueEntry->sender;
                controller->wait_interconnect_cb(queueEntry);
                }else if(type==MEMORY_OP_WRITE){
                    if(controller->is_lowest_private()){
                        *state=SIMPLE_MODIFIED;
                        queueEntry->dest=controller->get_directory();
                        queueEntry->line->state=SIMPLE_MODIFIED;
                        queueEntry->sendTo=queueEntry->sender;
                        controller->wait_interconnect_cb(queueEntry);
                    }else{
                    /*
                    *   handle as chache miss
                    */
                        queueEntry->line->state=SIMPLE_INVALID;
                        *state= SIMPLE_INVALID;
                        N_STAT_UPDATE(miss_state,[oldstate]++,kernel);
                        controller->cache_miss_cb(queueEntry);
                    }
                }
                break;
        default:
                memdebug("Invalid Line State"<< oldstate);
                assert(0);
		}
		if(oldstate!= *state){
		/*
		*   if cache line state has changed, we must refresh statistics
		*/
            UPDATE_SIMPLE_TRANS_STATS(oldstate,*state,kernel);
		}
    }

    void SimpleCoherentCache::handle_local_miss(CacheQueueEntry *queueEntry){
     memdebug("handle local miss\n");
	if (queueEntry->line){
	 queueEntry->line->state = SIMPLE_INVALID;
    }

    /* Go to directory if its lowest private and not UPDATE */
    if (controller->is_lowest_private() &&
            queueEntry->request->get_type() != MEMORY_OP_UPDATE) {
        queueEntry->dest = controller->get_directory();
    } else {
        queueEntry->dest = controller->get_lower_cont();
    }
    /*send Response*/
    queueEntry->eventFlags[CACHE_WAIT_INTERCONNECT_EVENT]++;
    queueEntry->sendTo = controller->get_lower_intrconn();
    controller->wait_interconnect_cb(queueEntry);
    }

    void SimpleCoherentCache::handle_interconn_hit(CacheQueueEntry *queueEntry){
     memdebug("handle interconn hit\n");
    /*
    * clear queueEntry only
    */
      controller->clear_entry_cb(queueEntry);
    }

    void SimpleCoherentCache::handle_interconn_miss(CacheQueueEntry *queueEntry){
    /*
    * clear queueEntry only
    */
      controller->clear_entry_cb(queueEntry);
    }

    void SimpleCoherentCache::handle_cache_insert(CacheQueueEntry *queueEntry, W64 oldTag){
             memdebug("handle cache insert\n");
            SIMPLECacheLineState *state=(SIMPLECacheLineState*) &queueEntry->line->state;
            SIMPLECacheLineState oldState= *state;

            /* if line is modified, we must uptade lower caches*/
            if(oldState == SIMPLE_MODIFIED){
                controller->send_update_to_lower(queueEntry,oldTag);
            }
            /* if we have to insert something, we must evict something from upper cache, when nothing is invalid*/
            if(oldState != SIMPLE_INVALID && controller->is_lowest_private()){
                controller->send_evict_to_upper(queueEntry,oldTag);
            }
            *state=SIMPLE_INVALID;
    }

    void SimpleCoherentCache::handle_cache_evict(CacheQueueEntry *queueEntry){
    /* do nothing*/
    memdebug("handle cache evict");
    }

    void SimpleCoherentCache::complete_request(CacheQueueEntry *queueEntry, Message &message){
    memdebug("handle complete request\n");
        assert(queueEntry->line);
        assert(message.hasData);

        if(controller->is_lowest_private()){
        SIMPLECacheLineState *state = (SIMPLECacheLineState*) &queueEntry->line->state;
                SIMPLECacheLineState oldState    = *state;
                OP_TYPE type                  = queueEntry->request->get_type();
                bool kernel                   = queueEntry->request->is_kernel();
                bool isShared                 = message.isShared;

                /*
                * handle operation types
                */
                if(type== MEMORY_OP_EVICT){
                 memdebug("handle complete request: MEMORY_OP_EVICT\n");
                    if(controller->is_lowest_private()){
                        controller->send_evict_to_upper(queueEntry);
                    }
                    UPDATE_SIMPLE_TRANS_STATS(oldState,SIMPLE_INVALID,kernel);
                    *state=SIMPLE_INVALID;

                }
                if(type== MEMORY_OP_UPDATE){
                 memdebug("handle complete request: MEMORY_OP_UPDATE\n");
                ptl_logfile << "Queueentry: " << *queueEntry << endl;
                assert(0);
                }

                switch(oldState){
                    case SIMPLE_INVALID:
                             memdebug("handle complete request: SIMPLE_INVALID\n");
                            if(isShared){
                                if(type==MEMORY_OP_READ){
                                    *state=SIMPLE_INVALID;
                                }else{
                                    assert(0);
                                }
                            }else{
                                switch(type){
                                    case MEMORY_OP_READ:
                                        *state=SIMPLE_EXCLUSIVE;
                                        break;
                                    case MEMORY_OP_WRITE:
                                        *state=SIMPLE_MODIFIED;
                                        break;
                                    case MEMORY_OP_EVICT:
                                        *state=SIMPLE_INVALID;
                                        break;
                                    default:
                                        assert(0);
                                }
                            }
                            break;
                    case SIMPLE_MODIFIED:
                     memdebug("handle complete request: SIMPLE_MODIFIED\n");
                            assert(0);
                            break;
                    case SIMPLE_EXCLUSIVE:
                     memdebug("handle complete request: SIMPLE_EXCLUSIVE\n");
                            if(isShared){
                                if(type==MEMORY_OP_READ){
                                    *state=SIMPLE_EXCLUSIVE;
                                }else{
                                    assert(0);
                                }
                            }else{
                                switch(type){
                                        case MEMORY_OP_READ:
                                            *state=SIMPLE_EXCLUSIVE;
                                            break;
                                        case MEMORY_OP_WRITE:
                                            *state=SIMPLE_MODIFIED;
                                            break;
                                        case MEMORY_OP_EVICT:
                                            if(controller->is_lowest_private()){
                                                controller->send_evict_to_upper(queueEntry);
                                            }
                                            *state=SIMPLE_INVALID;
                                            break;
                                        default:
                                            assert(0);
                                    }
                            }
                            break;
                    default:
                        memdebug("Invalid line state: " << oldState);
                        assert(0);

                }
                /* update stat*/
        if (oldState != *state) {
           UPDATE_SIMPLE_TRANS_STATS(oldState,*state,kernel);
        }

        }else{
             memdebug("handle complete request: else\n");
        if(message.request->get_type()==MEMORY_OP_EVICT){
                queueEntry->line->state=SIMPLE_INVALID;
            }else if(controller->is_private()){
                //message contains valid Argument
                queueEntry->line->state = *((SIMPLECacheLineState*)
                    message.arg);
            }else{
                //message from main memory
                queueEntry->line->state=SIMPLE_EXCLUSIVE;
            }
        }
    }

    void SimpleCoherentCache::handle_response(CacheQueueEntry *queueEntry, Message &message){
    /* nothing to do here*/
    memdebug("handle response\n");
    }

    bool SimpleCoherentCache::is_line_valid(CacheLine *line){
    /* check state of cache line*/
    memdebug("line valid");
        if(line->state==SIMPLE_INVALID){
            return false;
        }
        return true;
    }

    /*invalidate cache line*/
    void SimpleCoherentCache::invalidate_line(CacheLine *line){
    memdebug("invalidate cache line \n");
        line->state=SIMPLE_INVALID;
    }

    void SimpleCoherentCache::dump_configuration(YAML::Emitter &out)const{
        YAML_KEY_VAL(out,"cohrerence","SIMPLE")
    }

    /* SIMPLE Controler Builder*/

    struct SIMPLECacheControlerBuilder :public ControllerBuilder{
            /* Constructor*/
            SIMPLECacheControlerBuilder(const char *name): ControllerBuilder(name){}

            Controller* get_new_controller(W8 coreid, W8 type,MemoryHierarchy& mem, const char *name) {

                CacheController *cont = new CacheController(coreid, name, &mem, (Memory::CacheType)(type));

                SimpleCoherentCache *scoca= new SimpleCoherentCache( cont, cont->get_stats(), &mem);

                cont->set_coherence_logic(scoca);

                bool is_private = false;
                if (!mem.get_machine().get_option(name, "private", is_private)) {
                    is_private = false;
                }
                cont->set_private(is_private);

                bool is_lowest_private = false;
                if (!mem.get_machine().get_option(name, "last_private",
                        is_lowest_private)) {
                    is_lowest_private = false;
                }
                cont->set_lowest_private(is_lowest_private);

                return cont;
            }
    };

    SIMPLECacheControlerBuilder simpleCacheBuilder("simple_cache");
